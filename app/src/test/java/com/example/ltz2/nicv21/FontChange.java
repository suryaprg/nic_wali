package com.example.ltz2.nicv21;

import android.app.Application;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by LALA on 10/01/2019.
 */

public class FontChange extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/cartoonist_kooky.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        //....
    }
}
