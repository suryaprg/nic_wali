package com.example.ltz2.nicv21.Katalog;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;


import com.example.ltz2.nicv21.Barchart.BarchartKeuanganActivity;
import com.example.ltz2.nicv21.R;

public class KatalogKeuanganActivity extends Activity {
    LinearLayout mLinear;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.katalog_keuangan_new);
        mLinear = (LinearLayout) findViewById(R.id.keuangan_linear_grid);
        setSingleEvent(mLinear);

    }

    private void setSingleEvent(LinearLayout mLinear) {
        //Loop all child item of Main Grid
        for (int i = 0; i < mLinear.getChildCount(); i++) {
            //You can see , all child item is CardView , so we just cast object to CardView
            CardView cardView = (CardView) mLinear.getChildAt(i);
            final int finalI = i;
            if (i == 0) {
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(KatalogKeuanganActivity.this, BarchartKeuanganActivity.class);
                        intent.putExtra("info", "Total Pagu Pendapatan dan Realisasi");
                        intent.putExtra("index", finalI);
                        startActivity(intent);
                    }
                });
            } else if (i == 1) {
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(KatalogKeuanganActivity.this, BarchartKeuanganActivity.class);
                        intent.putExtra("info", "Total Realisasi tiap Bulan");
                        intent.putExtra("index", finalI);
                        startActivity(intent);
                    }
                });
            } else if (i == 2) {
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(KatalogKeuanganActivity.this, BarchartKeuanganActivity.class);
                        intent.putExtra("info", "Total Pagu Pendapatan tiap SKPD");
                        intent.putExtra("index", finalI);
                        startActivity(intent);
                    }
                });
            }
//            else if (i == 3) {
//                cardView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent intent = new Intent(KatalogKeuanganActivity.this, BarchartKeuanganActivity.class);
//                        intent.putExtra("info", "Realisasi");
//                        intent.putExtra("index", finalI);
//                        startActivity(intent);
//                    }
//                });
//            }
        }
    }


}
