package com.example.ltz2.nicv21.Katalog;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.LinearLayout;

import com.example.ltz2.nicv21.R;
import com.example.ltz2.nicv21.ViewPager.TabelPerpus_koleksi;
import com.example.ltz2.nicv21.ViewPager.TabelPerpus_pemakaman;
import com.example.ltz2.nicv21.ViewPager.TabelPerpus_statik;
import com.example.ltz2.nicv21.ViewPager.TabelPerum_pertamanan;

public class KatalogSaranaActivity extends Activity {
    LinearLayout mLinear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.katalog_sarana_new);
        mLinear = (LinearLayout) findViewById(R.id.sarana_linear_grid);
        setSingleEvent(mLinear);
    }

    private void setSingleEvent(LinearLayout mLinear) {
        //Loop all child item of Main Grid
        for (int i = 0; i < mLinear.getChildCount(); i++) {
            //You can see , all child item is CardView , so we just cast object to CardView
            final CardView cardView = (CardView) mLinear.getChildAt(i);
            final int finalI = i;
            if (i == 0) {
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(KatalogSaranaActivity.this, TabelPerpus_koleksi.class);
                        intent.putExtra("info", "Grafik Perpustakaan Koleksi");
                        intent.putExtra("index", "perpus_koleksi.json");
                        startActivity(intent);
                    }
                });
            } else if (i == 1) {
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(KatalogSaranaActivity.this, TabelPerpus_statik.class);
                        intent.putExtra("info", "Grafik Perpustakaan Statik Peminjaman");
                        intent.putExtra("index", "statik_peminjaman.json");
                        startActivity(intent);
                    }
                });
            } else if (i == 2) {
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(KatalogSaranaActivity.this, TabelPerum_pertamanan.class);
                        intent.putExtra("info", "Grafik Perumahan Pertamanan");
                        intent.putExtra("index", "perum_pertamanan2.json");
                        startActivity(intent);
                    }
                });
            } else if (i == 3) {
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(KatalogSaranaActivity.this, TabelPerpus_pemakaman.class);
                        intent.putExtra("info", "Grafik Perumahan Pemakaman");
                        intent.putExtra("index", "perum_pemakaman.json");
                        startActivity(intent);
                    }
                });
            }
        }
    }
}
