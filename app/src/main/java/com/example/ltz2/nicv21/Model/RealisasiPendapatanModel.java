package com.example.ltz2.nicv21.Model;

import android.support.annotation.NonNull;

public class RealisasiPendapatanModel implements Comparable<RealisasiPendapatanModel> {
    private double nominal;
    private int id_skpd;
    private int bulan;

    public RealisasiPendapatanModel(double nominal, int id_skpd, int bulan) {
        this.nominal = nominal;
        this.id_skpd = id_skpd;
        this.bulan = bulan;
    }

    public double getNominal() {
        return nominal;
    }

    public int getId_skpd() {
        return id_skpd;
    }

    public int getBulan() {
        return bulan;
    }

    @Override
    public int compareTo(RealisasiPendapatanModel obj) {
        return (this.getNominal() < obj.getNominal() ? -1 :
                (this.getNominal() == obj.getNominal() ? 0 : 1));
    }
}
