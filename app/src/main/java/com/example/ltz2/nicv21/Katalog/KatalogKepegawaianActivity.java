package com.example.ltz2.nicv21.Katalog;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ltz2.nicv21.Barchart.BarchartKepegawaianActivity;
import com.example.ltz2.nicv21.PieChart.PieChartKepegawaianActivity;
import com.example.ltz2.nicv21.R;

public class KatalogKepegawaianActivity extends Activity {
    LinearLayout mLinear;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.katalog_kepegawaian_new);
        mLinear = (LinearLayout) findViewById(R.id.kepegawaian_linear_grid);

        setSingleEvent(mLinear);
    }

    private void setSingleEvent(LinearLayout mLinear) {
        //Loop all child item of Main Grid
        for (int i = 0; i < mLinear.getChildCount(); i++) {
            //You can see , all child item is CardView , so we just cast object to CardView
            final CardView cardView = (CardView) mLinear.getChildAt(i);
            final int finalI = i;
            if (i == 0) {
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        textView = (TextView) findViewById(R.id.katalog_index_1);
                        Intent intent = new Intent(KatalogKepegawaianActivity.this, BarchartKepegawaianActivity.class);
                        intent.putExtra("info", "" + textView.getText());
//                        intent.putExtra("index", finalI);
                        startActivity(intent);
                    }
                });
            } else if (i == 1) {
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        textView = (TextView) findViewById(R.id.katalog_index_2);
                        Intent intent = new Intent(KatalogKepegawaianActivity.this, PieChartKepegawaianActivity.class);
                        intent.putExtra("info", "" + textView.getText());
//                        intent.putExtra("index", finalI);
                        startActivity(intent);
//                        Toast.makeText(KatalogKepegawaianActivity.this, "Data Masih belum ada", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }
}