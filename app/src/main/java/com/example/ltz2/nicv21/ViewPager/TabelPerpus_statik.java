package com.example.ltz2.nicv21.ViewPager;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.ltz2.nicv21.Adapter.Json_Reader;
import com.example.ltz2.nicv21.Model.Model_json_linear;
import com.example.ltz2.nicv21.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by LALA on 17/01/2019.
 */

public class TabelPerpus_statik extends Activity {
    String str_json;
    InputStream is;
    TableLayout main_table;
    TableRow row;
    TextView tv_th, tv_val, tv_val2, tv_a, tv_b, tv_c;

    ArrayList<Model_json_linear> data_list = new ArrayList<Model_json_linear>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tabelperpus_statik);
        String info = getIntent().getStringExtra("info");
        String index = getIntent().getStringExtra("index");
        TextView judulChart = (TextView) findViewById(R.id.judulChart);
        judulChart.setText(info);
        main_table = (TableLayout) findViewById(R.id.tablestatic);

        str_json = new Json_Reader().get_json_file(get_activity(), index);
        Log.e("tabel_perum_pertamanan", str_json);

        str_json = new Json_Reader().get_json_file(get_activity(), "perpus_statik.json");
        Log.e("tabel_perum_pertamanan", str_json);

        try {
            JSONObject data_json = new JSONObject(str_json);
            Integer leng_data = data_json.length();

            Iterator iterator = data_json.keys();
            int i = 0;
            while (iterator.hasNext()) {
                String key = (String) iterator.next();

                String val = (String) data_json.getJSONObject(key).getString("Parameter");
                String val2 = (String) data_json.getJSONObject(key).getString("Jumlah");


                Log.e("tabel_perum_pertamanan", "onCreate : jml_record: " + key + "value: " + val + "value: " + val2  );

                TableRow row = new TableRow(this);

                TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT);
                row.setLayoutParams(lp);

                tv_th = new TextView(this);
                tv_val = new TextView(this);
                tv_val2 = new TextView(this);


                tv_th.setText(key);
                tv_th.setWidth(250);
                tv_th.setBackgroundResource(R.color.white);


                tv_val.setText(val);
                tv_val.setWidth(250);
                tv_val.setBackgroundResource(R.color.white);

                tv_val2.setText(val2);
                tv_val2.setBackgroundResource(R.color.white);



                row.addView(tv_th);
                row.addView(tv_val);
                row.addView(tv_val2);

                main_table.addView(row, i);

                i++;

            }
            TableRow row1 = new TableRow(this);
            TableRow.LayoutParams lp2 = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT);
            row1.setLayoutParams(lp2);
            tv_a = new TextView(this);
            tv_b = new TextView(this);
            tv_c = new TextView(this);

            tv_a.setText("Tahun");
            tv_a.setWidth(200);
            tv_a.setBackgroundResource(R.color.honeydew);

            tv_b.setText("Parameter");
            tv_b.setWidth(200);
            tv_b.setBackgroundResource(R.color.teal);

            tv_c.setText("Jumlah");
            tv_c.setWidth(150);
            tv_c.setBackgroundResource(R.color.darkturqoise);

            row1.addView(tv_a);
            row1.addView(tv_b);
            row1.addView(tv_c);
            main_table.addView(row1, 0);

            Log.e("tabel_perwal", "onCreate : jml_record: " + leng_data);
        } catch (JSONException e) {
            Log.e("MainActivity", "Oncreate : " + e.getMessage());
        }
    }


    public Context get_activity() {
        Context context = this;
        return context;
    }
}
