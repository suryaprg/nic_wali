package com.example.ltz2.nicv21.Katalog;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.ltz2.nicv21.Adapter.Json_Reader;
import com.example.ltz2.nicv21.Adapter.ReadJSONUrl;
import com.example.ltz2.nicv21.Barchart.BarchartKemasyarakatanActivity;
import com.example.ltz2.nicv21.Barchart.BarchartKepegawaianActivity;
import com.example.ltz2.nicv21.Barchart.BarchartLegislasiActivity;
import com.example.ltz2.nicv21.MainActivity;
import com.example.ltz2.nicv21.Model.Model_json_linear;
import com.example.ltz2.nicv21.PieChart.PieChartKepegawaianActivity;
import com.example.ltz2.nicv21.R;
import com.example.ltz2.nicv21.TabelPerda;
import com.example.ltz2.nicv21.URLCollection;
import com.example.ltz2.nicv21.ViewPager.TabelPerwal;
import com.example.ltz2.nicv21.ViewPager.TabelPerwal_asli;
import com.example.ltz2.nicv21.ViewPager.TabelPerwal_paraf;
import com.example.ltz2.nicv21.ViewPager.TabelPerwal_salinan;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

public class KatalogLegislasiActivity extends Activity {
    LinearLayout mLinear;
//    String str_json;
//    InputStream is;
//    TableLayout main_table;
//    TableRow row;
//    TextView tv_th, tv_val, tv_a, tv_b;
//
//    ArrayList<Model_json_linear> data_list = new ArrayList<Model_json_linear>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.katalog_legislasi_new);
        mLinear = (LinearLayout) findViewById(R.id.legislasi_linear_grid);
        setSingleEvent(mLinear);
    }

    private void setSingleEvent(LinearLayout mLinear) {
        //Loop all child item of Main Grid
        for (int i = 0; i < mLinear.getChildCount(); i++) {
            //You can see , all child item is CardView , so we just cast object to CardView
            final CardView cardView = (CardView) mLinear.getChildAt(i);
            final int finalI = i;
            if (i == 0) {
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(KatalogLegislasiActivity.this, TabelPerda.class);
                        intent.putExtra("info", "Peraturan Daerah");
                        intent.putExtra("index", "perda.json");
                        startActivity(intent);
                    }
                });
            } else if (i == 1) {
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(KatalogLegislasiActivity.this, TabelPerwal.class);
                        intent.putExtra("info", "Peraturan Wali");
                        intent.putExtra("index", "perwal.json");
                        startActivity(intent);
                    }
                });
            } else if (i == 2) {
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(KatalogLegislasiActivity.this, TabelPerwal_asli.class);
                        intent.putExtra("info", "Peraturan Wali Asli");
                        intent.putExtra("index", "perwal_asli.json");
                        startActivity(intent);
                    }
                });
            } else if (i == 3) {
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(KatalogLegislasiActivity.this, TabelPerwal_paraf.class);
                        intent.putExtra("info", "Peraturan Wali Paraf");
                        intent.putExtra("index", "perwal_paraf.json");
                        startActivity(intent);
                    }
                });
            } else if (i == 4) {
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(KatalogLegislasiActivity.this, TabelPerwal_salinan.class);
                        intent.putExtra("info", "Peraturan Wali Salinan");
                        intent.putExtra("index", "perwal_salinan.json");
                        startActivity(intent);
                    }
                });

            } else if (i == 5) {
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(KatalogLegislasiActivity.this, BarchartLegislasiActivity.class);
                        intent.putExtra("info", "Coba Chart");
                        intent.putExtra("index", "grafik_perum_pemakaman.json");
                        startActivity(intent);
                    }
                });
            }
        }
    }
}

//        main_table = (TableLayout) findViewById(R.id.table);
//
//        str_json = new Json_Reader().get_json_file(get_activity(), "perda.json");
//
//        Log.e("katalog_legislasi_new", str_json);
////        mLinear = (LinearLayout) findViewById(R.id.legislasi_linear_grid);
////        setSingleEvent(mLinear);
//        try {
//            JSONObject data_json = new JSONObject(str_json);
//            Integer leng_data = data_json.length();
//
//            Iterator iterator = data_json.keys();
//            int i = 0;
//            while (iterator.hasNext()) {
//                String key = (String) iterator.next();
//
//                String val = (String) data_json.getString(key);
//                Log.e("katalog_legislasi_new", "onCreate : jml_record: " + key + "value: " + val);
//
//                TableRow row = new TableRow(this);
//
//                TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT);
//                row.setLayoutParams(lp);
//
//                tv_th = new TextView(this);
//                tv_val = new TextView(this);
//
//
//                tv_th.setText(key);
//                tv_th.setWidth(250);
//                tv_th.setBackgroundResource(R.color.white);
//
//
//                tv_val.setText(val);
//                tv_val.setWidth(250);
//                tv_val.setBackgroundResource(R.color.white);
//
//                row.addView(tv_th);
//                row.addView(tv_val);
//                main_table.addView(row, i);
//
//                i++;
//
//            }
//            TableRow row1 = new TableRow(this);
//            TableRow.LayoutParams lp2 = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT);
//            row1.setLayoutParams(lp2);
//            tv_a = new TextView(this);
//            tv_b = new TextView(this);
//
//            tv_a.setText("tahun");
//            tv_a.setWidth(250);
//            tv_a.setBackgroundResource(R.color.honeydew);
//
//            tv_b.setText("jumlah");
//            tv_b.setWidth(250);
//            tv_b.setBackgroundResource(R.color.teal);
//
//            row1.addView(tv_a);
//            row1.addView(tv_b);
//            main_table.addView(row1, 0);
//
//            Log.e("katalog_legislasi_new", "onCreate : jml_record: " + leng_data);
//        } catch (JSONException e) {
//            Log.e("MainActivity", "Oncreate : " + e.getMessage());
//        }
//    }
//
//
//    public Context get_activity() {
//        Context context = this;
//        return context;
//    }
