package com.example.ltz2.nicv21.Model;

public class UnitKerjaModel {
    //int per_unit_kerja
    int jml_gol_1;
    int jml_gol_2;
    int jml_gol_3;
    int jml_gol_4;
    int laki;
    int perempuan;
    int islam;
    int kristen;
    int katolik;
    int hindu;
    int budha;
    int kawin;
    int belum_kawin;
    String nama_unitKerja;

    public UnitKerjaModel(int jml_gol_1, int jml_gol_2, int jml_gol_3, int jml_gol_4, int laki, int perempuan, int islam, int kristen, int katolik, int hindu, int budha, int kawin, int belum_kawin, String nama) {
        this.jml_gol_1 = jml_gol_1;
        this.jml_gol_2 = jml_gol_2;
        this.jml_gol_3 = jml_gol_3;
        this.jml_gol_4 = jml_gol_4;
        this.laki = laki;
        this.perempuan = perempuan;
        this.islam = islam;
        this.kristen = kristen;
        this.katolik = katolik;
        this.hindu = hindu;
        this.budha = budha;
        this.kawin = kawin;
        this.belum_kawin = belum_kawin;
        this.nama_unitKerja = nama;
    }

    public String getNama_unitKerja() {
        return nama_unitKerja;
    }

    public int getTotalPegawai() {
        return laki + perempuan;
    }

    public int getJml_gol_1() {
        return jml_gol_1;
    }

    public int getJml_gol_2() {
        return jml_gol_2;
    }

    public int getJml_gol_3() {
        return jml_gol_3;
    }

    public int getJml_gol_4() {
        return jml_gol_4;
    }

    public int getLaki() {
        return laki;
    }

    public int getPerempuan() {
        return perempuan;
    }

    public int getIslam() {
        return islam;
    }

    public int getKristen() {
        return kristen;
    }

    public int getKatolik() {
        return katolik;
    }

    public int getHindu() {
        return hindu;
    }

    public int getBudha() {
        return budha;
    }

    public int getKawin() {
        return kawin;
    }

    public int getBelum_kawin() {
        return belum_kawin;
    }
}
