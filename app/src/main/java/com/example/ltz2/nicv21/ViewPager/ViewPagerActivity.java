//package com.example.ltz2.nicv21.ViewPager;
//
//import android.os.Bundle;
//import android.support.design.widget.TabLayout;
//import android.support.v4.view.ViewPager;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.Toolbar;
//import android.view.View;
//
//import com.example.ltz2.nicv21.R;
//
//
//public class ViewPagerActivity extends AppCompatActivity {
//
//    TabLayout tabLayout;
//    ViewPager viewPager;
//    ViewPagerAdapter viewPagerAdapter;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.view_pager);
//
//        tabLayout = (TabLayout) findViewById(R.id.tablayout);
//        viewPager = (ViewPager) findViewById(R.id.pager);
//        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
//        viewPager.setAdapter(viewPagerAdapter);
//        tabLayout.setupWithViewPager(viewPager);
//
//
//    }
//
////    @Override
////    public boolean onCreateOptionsMenu(Menu menu) {
////
////        getMenuInflater().inflate(R.menu.menu_main, menu);
////
////        return super.onCreateOptionsMenu(menu);
////    }
//
//
//}
