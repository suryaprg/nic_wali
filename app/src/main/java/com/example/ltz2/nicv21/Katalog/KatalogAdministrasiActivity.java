package com.example.ltz2.nicv21.Katalog;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.LinearLayout;

import com.example.ltz2.nicv21.Barchart.BarchartKemasyarakatanActivity;
import com.example.ltz2.nicv21.R;
import com.example.ltz2.nicv21.ViewPager.TabelDispenduk_agregat;
import com.example.ltz2.nicv21.ViewPager.TableDispenduk_akta;

public class KatalogAdministrasiActivity extends Activity {
    LinearLayout mLinear;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.katalog_administrasi_new);
        mLinear = (LinearLayout) findViewById(R.id.admin_linear_grid);
        setSingleEvent(mLinear);
    }

    private void setSingleEvent(LinearLayout mLinear) {
        //Loop all child item of Main Grid
        for (int i = 0; i < mLinear.getChildCount(); i++) {
            //You can see , all child item is CardView , so we just cast object to CardView
            final CardView cardView = (CardView) mLinear.getChildAt(i);
            final int finalI = i;
            if (i == 0) {
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(KatalogAdministrasiActivity.this, TabelDispenduk_agregat.class);
                        intent.putExtra("info", "Dinas Kependudukan Agregat Penduduk");
                        intent.putExtra("index", "dispenduk_agregat.json");
                        startActivity(intent);
                    }
                });
            } else if (i == 1) {
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(KatalogAdministrasiActivity.this, TableDispenduk_akta.class);
                        intent.putExtra("info", "Dinas Kependudukan Akta");
                        intent.putExtra("index", "dispenduk_akta2.json");
                        startActivity(intent);
                    }
                });
            } else if (i == 2) {
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent();
                        i.setAction(Intent.ACTION_VIEW);
                        i.addCategory(Intent.CATEGORY_BROWSABLE);

                        i.setData(Uri.parse("https://suradi.malangkota.go.id/admin/login"));
                        startActivity(i);
                    }
                });
            } else if (i == 3) {
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent();
                        i.setAction(Intent.ACTION_VIEW);
                        i.addCategory(Intent.CATEGORY_BROWSABLE);

                        i.setData(Uri.parse("http://ncc.sasindo.id/map?idsubmodule=8&selectlist%5Bkecamatan%5D%5B%5D=kecamatan&grouplist=kecamatan&joinlist%5B0%5D%5Bdest_table%5D=kelurahan&joinlist%5B0%5D%5Bdest_column%5D=idkelurahan&joinlist%5B1%5D%5Bdest_table%5D=kecamatan&joinlist%5B1%5D%5Bdest_column%5D=idkecamatan&joinlist%5B1%5D%5Bsrc_table%5D=kelurahan&joinlist%5B2%5D%5Bdest_table%5D=dinsos_kategoriresos&joinlist%5B2%5D%5Bdest_column%5D=idkategoriresos"));
                        startActivity(i);
                    }
                });
            } else if (i == 4) {
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent();
                        i.setAction(Intent.ACTION_VIEW);
                        i.addCategory(Intent.CATEGORY_BROWSABLE);

                        i.setData(Uri.parse("http://ncc.sasindo.id/map?idsubmodule=6&selectlist%5Bkelurahan%5D%5B%5D=kelurahan&grouplist=kelurahan&joinlist%5B0%5D%5Bdest_table%5D=kelurahan&joinlist%5B0%5D%5Bdest_column%5D=idkelurahan&joinlist%5B1%5D%5Bdest_table%5D=kecamatan&joinlist%5B1%5D%5Bdest_column%5D=idkecamatan&joinlist%5B1%5D%5Bsrc_table%5D=kelurahan&joinlist%5B2%5D%5Bdest_table%5D=hubungankeluarga&joinlist%5B2%5D%5Bdest_column%5D=idhubungankeluarga&joinlist%5B3%5D%5Bdest_table%5D=jeniskelamin&joinlist%5B3%5D%5Bdest_column%5D=idjeniskelamin&joinlist%5B4%5D%5Bdest_table%5D=statuskawin&joinlist%5B4%5D%5Bdest_column%5D=idstatuskawin&joinlist%5B5%5D%5Bdest_table%5D=pendidikan&joinlist%5B5%5D%5Bdest_column%5D=idpendidikan&joinlist%5B6%5D%5Bdest_table%5D=statuskeluarga&joinlist%5B6%5D%5Bdest_column%5D=idstatuskeluarga&joinlist%5B7%5D%5Bdest_table%5D=statusindividu&joinlist%5B7%5D%5Bdest_column%5D=idstatusindividu"));
                        startActivity(i);
                    }
                });
            }
        }
    }
}
