package com.example.ltz2.nicv21.Katalog;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.example.ltz2.nicv21.Adapter.RecyclerViewAdapter;
import com.example.ltz2.nicv21.R;
import com.example.ltz2.nicv21.ViewPager.ViewPagerAdapter;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class KatalogIMMActivity extends Activity {
    TextView textView3;
    TextView textView4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.katalog_imm);
        //RUN UI ON THREAD NEW METHOD
//        textView = (TextView) findViewById(R.id.tv_tStory_list);
        textView3 = (TextView) findViewById(R.id.tv_edisi_hari);
        textView4 = (TextView) findViewById(R.id.tv_edisi_tanggal);
        getWebsite();

    }

    private void getWebsite() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final StringBuilder builder = new StringBuilder();
//                final StringBuilder builder2 = new StringBuilder();
                final ArrayList<String> string_array_TopStories = new ArrayList<>();
                final ArrayList<String> string_array_TopPersons = new ArrayList<>();
                final ArrayList<String> string_array_date = new ArrayList<>();
                final ArrayList<String> string_array_img_url = new ArrayList<>();

                try {
                    Document doc = Jsoup.connect("https://imm.ebdesk.com/").timeout(10000).get();
//                    String title = doc.title();
////                    Elements links = doc.select("a[href]");
//
//                    builder.append(title).append("\n");
                    Elements div = doc.select("#demo1");
                    Elements ul = div.select("#demo1 > ul");
                    Elements elements = ul.select("li");
                    for (Element element : elements
                            ) {
                        string_array_TopStories.add(element.text());
                    }
//                    builder.append();
//                    for (Element link : links) {
//                        builder.append("\n").append("Link : ").append(link.attr("href"))
//                                .append("\n").append("Text : ").append(link.text());
//                    }
                    div = doc.select("div.hotissue-place");
                    ul = div.select("span");
                    for (Element element : ul
                            ) {
                        string_array_TopPersons.add(element.text());
                    }
                    //...
                    div = doc.select("div.dateheaderidx");
                    ul = div.select("span");
                    for (Element element : ul
                            ) {
                        string_array_date.add(element.text());
                    }
                    //...
                    div = doc.select("div.topstories");
                    Elements scriptEl = div.select("script");
                    for (Element element : scriptEl
                            ) {
                        Pattern pattern = Pattern.compile("images = ([^;]*)");
                        Matcher matcher = pattern.matcher(element.data());
                        if (matcher.find()) {
                            builder.append(matcher.group(1));
                            String str = builder.toString();
                            str = str.replace("[", "");
                            str = str.replace("]", "");
                            str = str.replace("\"", "");
                            String[] tempArray = str.split(",");
                            for (int i = 0; i < tempArray.length; i++) {
                                string_array_img_url.add(tempArray[i]);
                            }
//                            builder.delete(0, 1);
//                            builder.delete(builder.length() - 1, builder.length());
                            Log.d("Output coba url", string_array_img_url.toString());
                            break;

                        }
                    }

                } catch (Exception e) {
                    Log.d("Error GetWebsite", e.toString() + "");
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //                        textView.setText(string_array_TopStories.get(0));
//                        textView2.setText(string_array_TopPersons.get(0));
                        textView3.setText(string_array_date.get(0) + " " + string_array_date.get(1));
                        textView4.setText(string_array_date.get(2));
//                        textView.setText(builder.toString());
//                        textView2.setText(builder2.toString());
                        //TOP STORIES RECYCLE
                        RecyclerViewAdapter adapter = new RecyclerViewAdapter(string_array_TopStories);
                        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rec_view_tStories);
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getParent());
                        recyclerView.setLayoutManager(layoutManager);
                        recyclerView.setAdapter(adapter);
                        //TOP PERSONS
                        RecyclerViewAdapter adapter2 = new RecyclerViewAdapter(string_array_TopPersons);
                        RecyclerView recyclerView2 = (RecyclerView) findViewById(R.id.rec_view_tPersons);
                        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(getParent());
                        recyclerView2.setLayoutManager(layoutManager2);
                        recyclerView2.setAdapter(adapter2);
                        //
                        ViewPager viewPager = findViewById(R.id.view_pager_slide_img);
                        ViewPagerAdapter adapterVP = new ViewPagerAdapter(getApplicationContext(), string_array_img_url);
                        viewPager.setAdapter(adapterVP);
                    }
                });
            }
        }).start();
    }
}
