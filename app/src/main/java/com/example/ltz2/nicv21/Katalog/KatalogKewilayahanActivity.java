package com.example.ltz2.nicv21.Katalog;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.ltz2.nicv21.R;

public class KatalogKewilayahanActivity extends Activity {

    Button bt_kdkd, bt_klojen, bt_bli, bt_sukun, bt_lw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.katalog_kewilayahan_new);

        bt_kdkd = (Button) findViewById(R.id.bt_kdkd);
        bt_kdkd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);

                i.setData(Uri.parse("https://www.google.com/maps/place/Kedungkandang,+Kota+Malang,+Jawa+Timur/@-8.0189937,112.6162,12.46z/data=!4m5!3m4!1s0x2dd627d22b39369f:0xec18b0aca913f173!8m2!3d-8.0110139!4d112.6556213"));
                startActivity(i);
            }
        });

        bt_klojen = (Button) findViewById(R.id.bt_klojen);
        bt_klojen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);

                i.setData(Uri.parse("https://www.google.com/maps/place/Klojen,+Kota+Malang,+Jawa+Timur/@-7.97469,112.5908503,13z/data=!3m1!4b1!4m5!3m4!1s0x2dd628242a7d2d89:0x83021151613d2f94!8m2!3d-7.9770736!4d112.6258244"));
                startActivity(i);
            }
        });

        bt_bli = (Button) findViewById(R.id.bt_bli);
        bt_bli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);

                i.setData(Uri.parse("https://www.google.com/maps/place/Blimbing,+Kota+Malang,+Jawa+Timur/@-7.9535781,112.6170152,13z/data=!3m1!4b1!4m5!3m4!1s0x2dd629b10e70017d:0x5f74320fd5eba209!8m2!3d-7.9553448!4d112.649662"));
                startActivity(i);
            }
        });

        bt_lw = (Button) findViewById(R.id.bt_lw);
        bt_lw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);

                i.setData(Uri.parse("https://www.google.com/maps/place/Kec.+Lowokwaru,+Kota+Malang,+Jawa+Timur/@-7.93901,112.5708638,13z/data=!3m1!4b1!4m5!3m4!1s0x2e788212466171a9:0x24334f51be07e214!8m2!3d-7.9370268!4d112.6139055"));
                startActivity(i);
            }
        });

        bt_sukun = (Button) findViewById(R.id.bt_sukun);
        bt_sukun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);

                i.setData(Uri.parse("https://www.google.com/maps/place/Kec.+Lowokwaru,+Kota+Malang,+Jawa+Timur/@-7.93901,112.5708638,13z/data=!3m1!4b1!4m5!3m4!1s0x2e788212466171a9:0x24334f51be07e214!8m2!3d-7.9370268!4d112.6139055"));
                startActivity(i);
            }
        });
    }
}
