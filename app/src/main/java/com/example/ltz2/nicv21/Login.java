package com.example.ltz2.nicv21;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ltz2.nicv21.Adapter.DataHelper;
import com.example.ltz2.nicv21.Adapter.SessionManagement;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Login extends AppCompatActivity {

    DataHelper dbHelper; //membuat variabel dbHelper dari class DataHelper
    private Button loginBut; //membuat atribut untuk button login dan button logout
    private EditText user, pass; //membuat atribut untuk edit text user dan password
    SessionManagement sessionManagement; //membuat variabel session dari class sessionManagement
    Button buttonLogout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        user = (EditText) findViewById(R.id.etUsername); //instansiasi objek yang memanggil id edit text username
        pass = (EditText) findViewById(R.id.ePassword); //instansiasi objek yang memanggil id edit text password

        dbHelper = new DataHelper(this); //instansiasi objek datahelper

        sessionManagement = new SessionManagement(Login.this);

        if (sessionManagement.isLoggedIn()) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
        }
        loginBut = (Button) findViewById(R.id.logine); //instansiasi objek yang memanggil id button login

        loginBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = new Bundle();

                b.putString("username", user.getText().toString()); //memanggil kolom nama pada sql
                b.putString("password", pass.getText().toString()); //memanggil kolom password pada sql
                if(CekLogin()){

                    if (cekValidasi()) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class); //membut intent button login berpindah ke halaman Halo Activity
                        sessionManagement.createLoginSession(user.getText().toString(), pass.getText().toString()); //memanggil session untuk membaca username dan password
                        i.putExtras(b);
                        startActivity(i);
                        finish();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "You are not registed", Toast.LENGTH_SHORT).show(); //membuat noti
                }
            }
        });
    }

    public boolean CekLogin() {
        if (user.getText().toString().equals("wali") && pass.getText().toString().equals("12345")) {
            Toast.makeText(getApplicationContext(), "Hai, Bertemu lagi", Toast.LENGTH_SHORT).show();
            return true;
//        } else if (user.getText().toString().equals("")) {
//            Toast.makeText(getApplicationContext(), "Fill the Username", Toast.LENGTH_SHORT).show();
//            return false;
//        } else if (pass.getText().toString().equals("")) {
//            Toast.makeText(getApplicationContext(), "Fill the Password", Toast.LENGTH_SHORT).show();
//            return false;
        } else {
            return false;
        }
    }

    public boolean cekValidasi() {
        if (user.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Fill the Username", Toast.LENGTH_SHORT).show();
            return false;
        } else if (pass.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Fill the Password", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
