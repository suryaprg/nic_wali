package com.example.ltz2.nicv21.Adapter;

import android.content.Context;
import android.util.Log;

import com.example.ltz2.nicv21.Model.PaguPendatapanModel;
import com.example.ltz2.nicv21.Model.RealisasiPendapatanModel;
import com.example.ltz2.nicv21.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class KeuanganDataChart {
    public ArrayList<PaguPendatapanModel> PaguPendapatanList;
    public ArrayList<RealisasiPendapatanModel> RealisasiPendapatanList;
    JSONObject jsondata;

    public KeuanganDataChart(Context context) {
//        try {
//            jsondata = new JSONObject(getJsonData(context));
//            JSONArray jsonpagu = jsondata.getJSONArray("pagu_pendapatan");
//            Log.d("Dari Keuangan", "" + jsonpagu.getJSONObject(0).getString("NAMA_SKPD"));
//            JSONArray jsonrealisasi = jsondata.getJSONArray("realisasi_pendapatan");
//            for (int i = 0; i < jsonpagu.length(); i++) {
//                JSONObject temp = jsonpagu.getJSONObject(i);
//                Double nominal = temp.getDouble("NOMINAL");
//                int id = temp.getInt("ID_SKPD");
//                String nama = temp.getString("NAMA_SKPD");
//                PaguPendapatanList.add(new PaguPendatapanModel(nominal, id, nama));
//            }
//            for (int i = 0; i < jsonrealisasi.length(); i++) {
//                JSONObject temp = jsonrealisasi.getJSONObject(i);
//                Double nominal = temp.getDouble("NOMINAL");
//                int id = temp.getInt("ID_SKPD");
//                int bulan = temp.getInt("BULAN");
//                RealisasiPendapatanList.add(new RealisasiPendapatanModel(nominal, id, bulan));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            Log.d("JSON Parser", "String data to json error");
//        }
    }

//    public ArrayList<PaguPendatapanModel> getPagu() {
//        return PaguPendapatanList;
//    }
//
//    public ArrayList<RealisasiPendapatanModel> getRealisasi() {
//        return RealisasiPendapatanList;
//    }

    public String getJsonData(Context context) {
        String json = null;
        try {
//            InputStream is = context.getAssets().open(filename);
            InputStream is = context.getResources().openRawResource(R.raw.anggaran_dari_mmc);

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            Log.d("JSON Parser", "File JSON error");
            Log.d("EXCEPTION JSON", ex.toString());
            return null;
        }
        return json;
    }

}
