package com.example.ltz2.nicv21.Model;

import android.support.annotation.NonNull;

public class PaguPendatapanModel implements Comparable<PaguPendatapanModel> {
    private double nominal;
    private int id_skpd;
    private String nama_skpd;

    public PaguPendatapanModel(double nominal, int id_skpd, String nama_skpd) {
        this.nominal = nominal;
        this.id_skpd = id_skpd;
        this.nama_skpd = nama_skpd;
    }

    public double getNominal() {
        return nominal;
    }

    public int getId_skpd() {
        return id_skpd;
    }

    public String getNama_skpd() {
        switch (this.nama_skpd) {
            case "Dinas Komunikasi Dan Informatika":
                return "Diskominfo";
            case "Dinas Penanaman Modal Dan Pelayanan Terpadu Satu Pintu":
                return "DPMPTSP";
            case "Dinas Lingkungan Hidup":
                return "DLH";
            case "Dinas Perhubungan":
                return "Dishub";
            case "Dinas Pekerjaan Umum Dan Penataan Ruang":
                return "DPUPR";
            case "Dinas Perumahan Dan Kawasan Permukiman":
                return "Disperkim";
            case "Dinas Kesehatan":
                return "Dinkes";
            case "Dinas Pendidikan":
                return "Diknas";
            case "Dinas Kepemudaan Dan Olahraga":
                return "Dispora";
            case "Dinas Pertanian Dan Ketahanan Pangan":
                return "Disperta";
            case "Dinas Perdagangan":
                return "Disdag";
            case "Dinas Kebudayaan Dan Pariwisata":
                return "Disbudpar";
            case "Badan Pengelola Keuangan Dan Aset Daerah":
                return "BPKAD";
            case "Rumah Sakit Umum Daerah":
                return "RSUD";
            case "Badan Pelayanan Pajak Daerah":
                return "BP2D";
            case "Badan Pengelola Keuangan Dan Aset Daerah (Selaku PPKD)":
                return "BPKAD (PPKD)";

        }
        return nama_skpd;
    }

    @Override
    public String toString() {
        return nama_skpd;
    }

    @Override
    public int compareTo(PaguPendatapanModel obj) {
        return (this.getNominal() < obj.getNominal() ? -1 :
                (this.getNominal() == obj.getNominal() ? 0 : 1));
    }
}
