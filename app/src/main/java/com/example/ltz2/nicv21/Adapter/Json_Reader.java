package com.example.ltz2.nicv21.Adapter;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by LALA on 16/01/2019.
 */

public class Json_Reader {

    public String get_json_file(Context context, String name_file){
        String json_return = "";
        try {
            InputStream is = context.getAssets().open(name_file);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json_return = new String(buffer, "UTF-8");
        }catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json_return;
    }
}
