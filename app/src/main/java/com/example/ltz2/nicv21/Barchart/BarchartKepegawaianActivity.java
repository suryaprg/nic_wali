package com.example.ltz2.nicv21.Barchart;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ltz2.nicv21.Adapter.ReadJSONUrl;
import com.example.ltz2.nicv21.Model.UnitKerjaModel;
import com.example.ltz2.nicv21.R;
import com.example.ltz2.nicv21.URLCollection;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import javax.sql.DataSource;

public class BarchartKepegawaianActivity extends Activity {
    JSONObject jsondata;
    BarChart barChart;
    Spinner spinner1, spinner2;
    String urlData;
    ArrayList<String> label;
    ArrayList<BarEntry> barEntries;
    ArrayList<UnitKerjaModel> dbUnitKerja;
    ArrayList<String> tempUnitkerja;

    int index_UnitKerja;
    int index;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.barchart_graph2);
        barChart = (BarChart) findViewById(R.id.bargraph);
        spinner1 = (Spinner) findViewById(R.id.spinner1);
        spinner2 = (Spinner) findViewById(R.id.spinner2);
        String info = getIntent().getStringExtra("info");
        index_UnitKerja = 0;
        index = 0;

        TextView judulChart = (TextView) findViewById(R.id.judulChart);
        judulChart.setText(info);
        barEntries = new ArrayList<>();
        label = new ArrayList<>();
        dbUnitKerja = new ArrayList<>();
        new BarchartKepegawaianActivity.GetURLdata().execute(URLCollection.DATA_SOURCE_KEPEGAWAIAN);

    }

    public void fillSpinner() {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tempUnitkerja);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(dataAdapter);
        ArrayAdapter<CharSequence> dataAdapter2 = ArrayAdapter.createFromResource(this, R.array.kepegawaianFilter, android.R.layout.simple_spinner_item);
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinner2.setAdapter(dataAdapter2);
        spinner2.setOnItemSelectedListener(new CustomOnClickListener());
        spinner1.setOnItemSelectedListener(new CustomOnClickListener());
    }

    public void getDataChart() {
        try {
            tempUnitkerja = new ArrayList<>();
            for (int i = 0; i < jsondata.getJSONArray("var_item").length(); i++) {
                String nama = jsondata.getJSONArray("var_item").getJSONObject(i).getString("unit_kerja");
                int gol1 = jsondata.getJSONArray("var_item").getJSONObject(i).getInt("jumlah_golongan_I");
                int gol2 = jsondata.getJSONArray("var_item").getJSONObject(i).getInt("jumlah_golongan_II");
                int gol3 = jsondata.getJSONArray("var_item").getJSONObject(i).getInt("jumlah_golongan_III");
                int gol4 = jsondata.getJSONArray("var_item").getJSONObject(i).getInt("jumlah_golongan_IV");
                int laki = jsondata.getJSONArray("var_item").getJSONObject(i).getInt("laki");
                int perempuan = jsondata.getJSONArray("var_item").getJSONObject(i).getInt("perempuan");
                int islam = jsondata.getJSONArray("var_item").getJSONObject(i).getInt("agama_islam");
                int kristen = jsondata.getJSONArray("var_item").getJSONObject(i).getInt("agama_kristen");
                int katholik = jsondata.getJSONArray("var_item").getJSONObject(i).getInt("agama_katholik");
                int hindu = jsondata.getJSONArray("var_item").getJSONObject(i).getInt("agama_hindu");
                int budha = jsondata.getJSONArray("var_item").getJSONObject(i).getInt("agama_budha");
                int kawin = jsondata.getJSONArray("var_item").getJSONObject(i).getInt("status_kawin");
                int belum_kawin = jsondata.getJSONArray("var_item").getJSONObject(i).getInt("status_belum_kawin");
                dbUnitKerja.add(new UnitKerjaModel(gol1, gol2, gol3, gol4, laki, perempuan, islam, kristen, katholik, hindu, budha, kawin, belum_kawin, nama));
                tempUnitkerja.add(dbUnitKerja.get(i).getNama_unitKerja());
            }
            fillSpinner();
        } catch (Exception e) {
        }
    }

    public void showChart1() {

        barEntries.add(new BarEntry(0, (float) dbUnitKerja.get(index_UnitKerja).getJml_gol_2()));
        barEntries.add(new BarEntry(1, (float) dbUnitKerja.get(index_UnitKerja).getJml_gol_2()));
        barEntries.add(new BarEntry(2, (float) dbUnitKerja.get(index_UnitKerja).getJml_gol_3()));
        barEntries.add(new BarEntry(3, (float) dbUnitKerja.get(index_UnitKerja).getJml_gol_4()));
        label.add("Gol I");
        label.add("Gol II");
        label.add("Gol III");
        label.add("Gol IV");

    }

    public void showChart2() {
        barEntries.add(new BarEntry(0, (float) dbUnitKerja.get(index_UnitKerja).getLaki()));
        barEntries.add(new BarEntry(1, (float) dbUnitKerja.get(index_UnitKerja).getPerempuan()));
        label.add("Laki - Laki");
        label.add("Perempuan");
    }

    public void showChart3() {

        barEntries.add(new BarEntry(0, (float) dbUnitKerja.get(index_UnitKerja).getIslam()));
        barEntries.add(new BarEntry(1, (float) dbUnitKerja.get(index_UnitKerja).getKristen()));
        barEntries.add(new BarEntry(2, (float) dbUnitKerja.get(index_UnitKerja).getKatolik()));
        barEntries.add(new BarEntry(3, (float) dbUnitKerja.get(index_UnitKerja).getHindu()));
        barEntries.add(new BarEntry(4, (float) dbUnitKerja.get(index_UnitKerja).getBudha()));
        label.add("Islam");
        label.add("Kristen");
        label.add("Katholik");
        label.add("Hindu");
        label.add("Budha");
    }

    public void showChart4() {

        barEntries.add(new BarEntry(0, (float) dbUnitKerja.get(index_UnitKerja).getKawin()));
        barEntries.add(new BarEntry(1, (float) dbUnitKerja.get(index_UnitKerja).getBelum_kawin()));
        label.add("Sudah Kawin");
        label.add("Belum Kawin");
    }

    public void DrawChart() {
        barEntries.clear();
        label.clear();
        barChart.invalidate();
        barChart.clear();
        switch (index) {
            case 0:
                showChart1();
                break;
            case 1:
                showChart2();
                break;
            case 2:
                showChart3();
                break;
            case 3:
                showChart4();
                break;
        }

        BarDataSet set = new BarDataSet(barEntries, "Data Jumlah Pegawai");
        set.setColors(ColorTemplate.COLORFUL_COLORS);
        BarData data = new BarData(set);
        data.setValueFormatter(new IValueFormatter(){
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                return (int)value + "";
            }
        });
        data.setValueTextSize(12f);
        data.setBarWidth(0.4f); // set custom bar width
        barChart.setData(data);
        Legend legend = barChart.getLegend();
//        label.toArray(new String[label.size()]);
//        legend.setCustom();
        //label variabel
        legend.setEnabled(false);

        barChart.setFitBars(true); // make the x-axis fit exactly all bars

        barChart.notifyDataSetChanged();
        barChart.invalidate(); // refresh

        //grid line
        barChart.getXAxis().setDrawGridLines(false);
        barChart.getAxisLeft().setEnabled(false);
        barChart.getAxisRight().setEnabled(false);

        //set variable label array x or y
        barChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(label));

        //set label on chart
        barChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);

        //jml label view
        barChart.getXAxis().setLabelCount(label.size());
        barChart.getXAxis().setTextSize(12f);

        //label bawah
        barChart.getDescription().setEnabled(true);
        barChart.animateY(3000);
        Log.d("CEK DATA JSON", "" + (float) dbUnitKerja.get(0).getJml_gol_3());
    }

    public class CustomOnClickListener implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            index = spinner2.getSelectedItemPosition();
            index_UnitKerja = spinner1.getSelectedItemPosition();
            DrawChart();
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    public class GetURLdata extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {
            ReadJSONUrl downloadURL = new ReadJSONUrl();
            try {
                urlData = downloadURL.readUrl(url[0]);
                jsondata = new JSONObject(urlData);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return urlData;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            getDataChart();
            DrawChart();
        }
    }


}