//{ ALL Pagu
//        for (int i = 0; i < pagupendapatanlist.size(); i++) {
//            double value = pagupendapatanlist.get(i).getNominal();
//            barEntries.add(new BarEntry(i, (float) (value * 100 / jmlpagu)));
////            for (int j = 0; j <pagupendapatanlist.size(); j++) {
////                if(realisasipendapatanlist.get(i).getId_skpd()==pagupendapatanlist.get(j).getId_skpd()){
////                    label.add(pagupendapatanlist.get(j).getNama_skpd());
////                }
////            }
//            label.add(pagupendapatanlist.get(i).getNama_skpd());
//        }
//        BarDataSet set = new BarDataSet(barEntries, "urlData Dalam bentuk Rupiah");
//        set.setValueTextSize(12f);
//        BarData data = new BarData(set);
//        data.setBarWidth(0.4f); // set custom bar width
//        barChart.setData(data);
//        } ALL PAGU

//{ urlData Total Pagu dan Total Realisasi
//        barEntries.add(new BarEntry(0, (float) jmlpagu));
//        barEntries.add(new BarEntry(1, (float) jmlrealisasi));
//        label.add("Pagu Pendapatan");
//        label.add("Realisasi Pendapatan");
//        BarDataSet set = new BarDataSet(barEntries, "urlData Dalam bentuk Rupiah");
//        BarData data = new BarData(set);
//        data.setBarWidth(0.4f); // set custom bar width
//        barChart.setData(data);
//} urlData Total Pagu dan Total Realisasi

//{ urlData Realisasi per Bulan
//        barEntries.add(new BarEntry(0, (float)jmlrealisasibln[0]));
//        barEntries.add(new BarEntry(1, (float)jmlrealisasibln[1]));
//        barEntries.add(new BarEntry(2, (float)jmlrealisasibln[2]));
//        label.add("Bulan 1");
//        label.add("Bulan 2");
//        label.add("Bulan 3");
//        BarDataSet set = new BarDataSet(barEntries, "urlData Dalam bentuk Rupiah");
//        set.setValueTextSize(12f);
//        BarData data = new BarData(set);
//        data.setBarWidth(0.4f); // set custom bar width
//        barChart.setData(data);
//} urlData Realisasi per Bulan
package com.example.ltz2.nicv21.Barchart;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.example.ltz2.nicv21.Adapter.ReadJSONFile;
import com.example.ltz2.nicv21.Model.PaguPendatapanModel;
import com.example.ltz2.nicv21.Model.RealisasiPendapatanModel;
import com.example.ltz2.nicv21.R;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import org.json.JSONArray;
import org.json.JSONObject;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.renderer.XAxisRenderer;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.util.ArrayList;

public class
BarchartKeuanganActivity extends Activity {
    HorizontalBarChart barChart;
    ArrayList<String> label;
    ArrayList<BarEntry> barEntries;
    ArrayList<PaguPendatapanModel> pagupendapatanlist;
    ArrayList<RealisasiPendapatanModel> realisasipendapatanlist;
    double jmlpagu = 0;
    double jmlrealisasi = 0;
    double jmlrealisasibln[] = new double[3];
    JSONObject jsondata;
    int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.barchart_graph_horizontal);
        String info = getIntent().getStringExtra("info");
        int index = getIntent().getIntExtra("index", 9);
        TextView judulChart = (TextView) findViewById(R.id.judulChart);
        judulChart.setText(info);
        barChart = (HorizontalBarChart) findViewById(R.id.barchart_graph);
        barEntries = new ArrayList<>();
        label = new ArrayList<>();
        ReadJSONFile readJSON = new ReadJSONFile(this);
        hitungChart(this, readJSON);

        switch (index) {
            case 0:
                pagurealChart();
                break;
            case 1:
                allrealChart();
                break;
            case 2:
                paguChart();
                break;
        }
        barChart.getDescription().setEnabled(false);
        barChart.setFitBars(true); // make the x-axis fit exactly all bars
        barChart.invalidate(); // refresh
        barChart.getXAxis().setDrawGridLines(false);
        barChart.getAxisLeft().setEnabled(false);
        barChart.getAxisRight().setEnabled(false);
        barChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(label));
        barChart.getXAxis().setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return label.get((int) value);
            }
        });
        barChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
//        barChart.getXAxis().setLabelCount(label.size());
//        barChart.getXAxis().setLabelRotationAngle(0f);
        barChart.animateY(1500);
        barChart.getXAxis().setTextSize(12f);
        //coba marker
        MarkerView mv = new MarkerView(this, R.layout.bubble_textview);
        barChart.setMarker(mv);
        //
//        mv.refreshContent(barChart.getEntryByTouchPoint());
        Log.d("bulan 3", "" + count);
    }

    public void pagurealChart() {
        barEntries.add(new BarEntry(0, (float) jmlpagu));
        barEntries.add(new BarEntry(1, (float) jmlrealisasi));
        label.add("Pagu Pendapatan");
        label.add("Realisasi Pendapatan");
        BarDataSet set = new BarDataSet(barEntries, "Data Dalam bentuk Rupiah");
        set.setColors(ColorTemplate.COLORFUL_COLORS);
        BarData data = new BarData(set);
        data.setValueTextSize(12f);
        data.setBarWidth(0.4f); // set custom bar width
        barChart.setData(data);
        barChart.getXAxis().setLabelCount(label.size());
    }

    public void paguChart() {
        for (int i = 0; i < pagupendapatanlist.size(); i++) {
            double value = pagupendapatanlist.get(i).getNominal();
            Log.d("Pagu ke-" + i, "" + pagupendapatanlist.get(i).getNama_skpd());
            barEntries.add(new BarEntry(i, (float) (value * 100 / jmlpagu)));
//            for (int j = 0; j <pagupendapatanlist.size(); j++) {
//                if(realisasipendapatanlist.get(i).getId_skpd()==pagupendapatanlist.get(j).getId_skpd()){
//                    label.add(pagupendapatanlist.get(j).getNama_skpd());
//                }
//            }
            label.add(pagupendapatanlist.get(i).getNama_skpd());
        }
        BarDataSet set = new BarDataSet(barEntries, "Data Dalam bentuk Rupiah");
        set.setColors(ColorTemplate.COLORFUL_COLORS);
//        set.setValueTextSize(12f);
        BarData data = new BarData(set);
        data.setValueTextSize(12f);
        data.setBarWidth(0.6f); // set custom bar width
        barChart.setData(data);
//        barChart.getXAxis().setLabelRotationAngle(-45);
//        barChart.setVisibleXRangeMaximum(barEntries.size());
//        barChart.setVisibleYRangeMaximum(1, YAxis.AxisDependency.LEFT);
        barChart.setVisibleXRangeMaximum(7);
        barChart.getXAxis().setLabelCount(7);
//        barChart.setVisibleYRangeMaximum(4, YAxis.AxisDependency.LEFT);
//        barChart.getLegend().setWordWrapEnabled(true);
//        barChart.setXAxisRenderer(new CustomXAxisRenderer(barChart.getViewPortHandler(), barChart.getXAxis(), barChart.getTransformer(YAxis.AxisDependency.LEFT)));

    }

    public void allrealChart() {
        barEntries.add(new BarEntry(0, (float) jmlrealisasibln[0]));
        barEntries.add(new BarEntry(1, (float) jmlrealisasibln[1]));
        barEntries.add(new BarEntry(2, (float) jmlrealisasibln[2]));
        label.add("Bulan 1");
        label.add("Bulan 2");
        label.add("Bulan 3");
        BarDataSet set = new BarDataSet(barEntries, "Data Dalam bentuk Rupiah");
        set.setColors(ColorTemplate.COLORFUL_COLORS);
//        set.setValueTextSize(12f);
        BarData data = new BarData(set);
        data.setValueTextSize(12f);
        data.setBarWidth(0.4f); // set custom bar width
        barChart.setData(data);
        barChart.getXAxis().setLabelCount(label.size());
    }

    public void hitungChart(Context context, ReadJSONFile kdtc) {
        try {
            pagupendapatanlist = new ArrayList<>();
            realisasipendapatanlist = new ArrayList<>();
            jsondata = new JSONObject(kdtc.getJsonData(context));
            JSONArray jsonpagu = jsondata.getJSONArray("pagu_pendapatan");
            JSONArray jsonrealisasi = jsondata.getJSONArray("realisasi_pendapatan");
            Log.d("test", "" + jsonpagu.length());
            for (int i = 0; i < jsonpagu.length(); i++) {
                JSONObject temp = jsonpagu.getJSONObject(i);
                Double nominal = temp.getDouble("NOMINAL");
                int id = temp.getInt("ID_SKPD");
                String nama = temp.getString("NAMA_SKPD");
                pagupendapatanlist.add(new PaguPendatapanModel(nominal, id, nama));
                jmlpagu += nominal;
            }
            for (int i = 0; i < jsonrealisasi.length(); i++) {
                JSONObject temp = jsonrealisasi.getJSONObject(i);
                Double nominal = temp.getDouble("NOMINAL");
                int id = temp.getInt("ID_SKPD");
                int bulan = temp.getInt("BULAN");
                realisasipendapatanlist.add(new RealisasiPendapatanModel(nominal, id, bulan));
                jmlrealisasi += nominal;
                jmlrealisasibln[bulan - 1] += nominal;
                if (bulan == 3) {
                    count++;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("JSON Parser", "String data to json error");
        }
    }

//    public class CustomMarkerView extends MarkerView {
//
//        private TextView tvContent;
//
//        public CustomMarkerView(Context context, int layoutResource) {
//            super(context, layoutResource);
//            // this markerview only displays a textview
//            tvContent = (TextView) findViewById(R.id.tvContent);
//        }
//
//        // callbacks everytime the MarkerView is redrawn, can be used to update the
//        // content (user-interface)
//        @Override
//        public void refreshContent(Entry e, Highlight highlight) {
//            tvContent.setText("" + e.getY()); // set the entry-value as the display text
//        }
//    }
//
//    public class CustomXAxisRenderer extends XAxisRenderer {
//        public CustomXAxisRenderer(ViewPortHandler viewPortHandler, XAxis xAxis, Transformer trans) {
//            super(viewPortHandler, xAxis, trans);
//        }
//
//        @Override
//        protected void drawLabel(Canvas c, String formattedLabel, float x, float y, MPPointF anchor, float angleDegrees) {
//            String line[] = formattedLabel.split("\n");
//            Log.d("test custom format", line[0]);
//            Utils.drawXAxisValue(c, line[0], x, y, mAxisLabelPaint, anchor, angleDegrees);
//            Utils.drawXAxisValue(c, line[1], x + mAxisLabelPaint.getTextSize(), y + mAxisLabelPaint.getTextSize(), mAxisLabelPaint, anchor, angleDegrees);
//        }
//    }
}
