package com.example.ltz2.nicv21.Katalog;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.LinearLayout;

import com.example.ltz2.nicv21.Barchart.BarchartKemasyarakatanActivity;
import com.example.ltz2.nicv21.R;

public class KatalogPelayananActivity extends Activity {
    LinearLayout mLinear;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.katalog_pelayanan_new);
        mLinear = (LinearLayout) findViewById(R.id.pelayanan_linear_grid);
        setSingleEvent(mLinear);
    }

    private void setSingleEvent(LinearLayout mLinear) {
        //Loop all child item of Main Grid
        for (int i = 0; i < mLinear.getChildCount(); i++) {
            //You can see , all child item is CardView , so we just cast object to CardView
            final CardView cardView = (CardView) mLinear.getChildAt(i);
            final int finalI = i;
            if (i == 0) {
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(KatalogPelayananActivity.this, BarchartKemasyarakatanActivity.class);
                        intent.putExtra("info", "Perda");
                        intent.putExtra("index", finalI);
                        startActivity(intent);
                    }
                });
            } else if (i == 1) {
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(KatalogPelayananActivity.this, BarchartKemasyarakatanActivity.class);
                        intent.putExtra("info", "Perwal");
                        intent.putExtra("index", finalI);
                        startActivity(intent);
                    }
                });
            }
        }
    }
}
