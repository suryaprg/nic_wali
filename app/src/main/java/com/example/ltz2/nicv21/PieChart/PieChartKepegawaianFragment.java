//package com.example.ltz2.nicv21.PieChart;
//
//import android.app.Fragment;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.View;
//import android.widget.AdapterView;
//import android.widget.ArrayAdapter;
//import android.widget.Spinner;
//import android.widget.TextView;
//
//import com.example.ltz2.nicv21.Adapter.ReadJSONUrl;
//import com.example.ltz2.nicv21.Model.UnitKerjaModel;
//import com.example.ltz2.nicv21.R;
//import com.example.ltz2.nicv21.URLCollection;
//import com.github.mikephil.charting.charts.PieChart;
//import com.github.mikephil.charting.data.PieData;
//import com.github.mikephil.charting.data.PieDataSet;
//import com.github.mikephil.charting.data.PieEntry;
//import com.github.mikephil.charting.utils.ColorTemplate;
//
//import org.json.JSONObject;
//
//import java.io.IOException;
//import java.util.ArrayList;
//
//public class PieChartKepegawaianFragment extends Fragment {
//    PieChart pieChart;
//    JSONObject jsondata;
//    Spinner spinner1, spinner2;
//    String urlData;
//    //    ArrayList<String> label;
//    ArrayList<UnitKerjaModel> dbUnitKerja;
//    ArrayList<String> tempUnitkerja;
//    ArrayList<PieEntry> entry;
//
//    int index_UnitKerja;
//    int index;
//
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.piechart_graph);
//        String info = getIntent().getStringExtra("info");
//        index_UnitKerja = 0;
//        index = 0;
//        TextView judulChart = (TextView) findViewById(R.id.judulChart);
//        judulChart.setText(info);
////        pieChart = new PieChart(this);
//        pieChart = (PieChart) findViewById(R.id.piechartgraph);
//        spinner1 = (Spinner) findViewById(R.id.spinner_kiri);
//        spinner2 = (Spinner) findViewById(R.id.spinner_kanan);
//        entry = new ArrayList<>();
////        label = new ArrayList<>();
//        dbUnitKerja = new ArrayList<>();
//        new PieChartKepegawaianActivity.GetURLdata().execute(URLCollection.DATA_SOURCE_KEPEGAWAIAN);
//
//    }
//
//    public void getDataChart() {
//        try {
//            tempUnitkerja = new ArrayList<>();
//            for (int i = 0; i < jsondata.getJSONArray("var_item").length(); i++) {
//                String nama = jsondata.getJSONArray("var_item").getJSONObject(i).getString("unit_kerja");
//                int gol1 = jsondata.getJSONArray("var_item").getJSONObject(i).getInt("jumlah_golongan_I");
//                int gol2 = jsondata.getJSONArray("var_item").getJSONObject(i).getInt("jumlah_golongan_II");
//                int gol3 = jsondata.getJSONArray("var_item").getJSONObject(i).getInt("jumlah_golongan_III");
//                int gol4 = jsondata.getJSONArray("var_item").getJSONObject(i).getInt("jumlah_golongan_IV");
//                int laki = jsondata.getJSONArray("var_item").getJSONObject(i).getInt("laki");
//                int perempuan = jsondata.getJSONArray("var_item").getJSONObject(i).getInt("perempuan");
//                int islam = jsondata.getJSONArray("var_item").getJSONObject(i).getInt("agama_islam");
//                int kristen = jsondata.getJSONArray("var_item").getJSONObject(i).getInt("agama_kristen");
//                int katholik = jsondata.getJSONArray("var_item").getJSONObject(i).getInt("agama_katholik");
//                int hindu = jsondata.getJSONArray("var_item").getJSONObject(i).getInt("agama_hindu");
//                int budha = jsondata.getJSONArray("var_item").getJSONObject(i).getInt("agama_budha");
//                int kawin = jsondata.getJSONArray("var_item").getJSONObject(i).getInt("status_kawin");
//                int belum_kawin = jsondata.getJSONArray("var_item").getJSONObject(i).getInt("status_belum_kawin");
//                dbUnitKerja.add(new UnitKerjaModel(gol1, gol2, gol3, gol4, laki, perempuan, islam, kristen, katholik, hindu, budha, kawin, belum_kawin, nama));
//                tempUnitkerja.add(dbUnitKerja.get(i).getNama_unitKerja());
//            }
//            fillSpinner();
//        } catch (Exception e) {
//        }
//    }
//
//    public void DrawChart() {
//        entry.clear();
////        label.clear();
//        pieChart.invalidate();
//        pieChart.clear();
//        switch (index) {
//            case 0:
//                showChart1();
//                break;
//            case 1:
//                showChart2();
//                break;
//            case 2:
//                showChart3();
//                break;
//            case 3:
//                showChart4();
//                break;
//        }
////        for (int i = 0; i < entry.size(); i++) {
////            if (entry.get(0).getValue() < 1) {
////                entry.get(i).
////            }
////        }
//
//        PieDataSet set = new PieDataSet(entry, "Data Jumlah Pegawai");
//        set.setColors(ColorTemplate.COLORFUL_COLORS);
//        PieData data = new PieData(set);
////        Legend legend = pieChart.getLegend()
////        legend.setEnabled(true);
////        legend.setCustom(ColorTemplate.VORDIPLOM_COLORS, new String[] { "aaaaa", "bbbbb", "ccccc"});
//        data.setValueTextSize(12f);
//        pieChart.setData(data);
//        pieChart.setDrawEntryLabels(false);
//        pieChart.notifyDataSetChanged();
//        pieChart.invalidate(); // refresh
//        pieChart.getDescription().setEnabled(false);
//        pieChart.animateY(3000);
//        Log.d("CEK DATA JSON", "" + (float) dbUnitKerja.get(0).getJml_gol_3());
//    }
//
//    public void showChart1() {
//
//        entry.add(new PieEntry((float) dbUnitKerja.get(index_UnitKerja).getJml_gol_1(), "Gol I"));
//        entry.add(new PieEntry((float) dbUnitKerja.get(index_UnitKerja).getJml_gol_2(), "Gol II"));
//        entry.add(new PieEntry((float) dbUnitKerja.get(index_UnitKerja).getJml_gol_3(), "Gol III"));
//        entry.add(new PieEntry((float) dbUnitKerja.get(index_UnitKerja).getJml_gol_4(), "Gol 1V"));
////        label.add("Gol I");
////        label.add("Gol II");
////        label.add("Gol III");
////        label.add("Gol IV");
//
//    }
//
//    public void showChart2() {
//        entry.add(new PieEntry((float) dbUnitKerja.get(index_UnitKerja).getLaki(), "Laki - Laki"));
//        entry.add(new PieEntry((float) dbUnitKerja.get(index_UnitKerja).getPerempuan(), "Perempuan"));
////            label.add("Laki - Laki");
////            label.add("Perempuan");
//    }
//
//    public void showChart3() {
//
//        entry.add(new PieEntry((float) dbUnitKerja.get(index_UnitKerja).getIslam(), "Islam"));
//        entry.add(new PieEntry((float) dbUnitKerja.get(index_UnitKerja).getKristen(), "Kristen"));
//        entry.add(new PieEntry((float) dbUnitKerja.get(index_UnitKerja).getKatolik(), "Katholik"));
//        entry.add(new PieEntry((float) dbUnitKerja.get(index_UnitKerja).getHindu(), "Hindu"));
//        entry.add(new PieEntry((float) dbUnitKerja.get(index_UnitKerja).getBudha(), "Budha"));
////            label.add("Islam");
////            label.add("Kristen");
////            label.add("Katholik");
////            label.add("Hindu");
////            label.add("Budha");
//    }
//
//    public void showChart4() {
//
//        entry.add(new PieEntry((float) dbUnitKerja.get(index_UnitKerja).getKawin(), "Sudah Kawin"));
//        entry.add(new PieEntry((float) dbUnitKerja.get(index_UnitKerja).getBelum_kawin(), "Belum Kawin"));
////            label.add("Sudah Kawin");
////            label.add("Belum Kawin");
//    }
//
//    public void fillSpinner() {
//        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tempUnitkerja);
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinner1.setAdapter(dataAdapter);
//        ArrayAdapter<CharSequence> dataAdapter2 = ArrayAdapter.createFromResource(  this, R.array.kepegawaianFilter, android.R.layout.simple_spinner_item);
//        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_item);
//        spinner2.setAdapter(dataAdapter2);
//        spinner2.setOnItemSelectedListener(new PieChartKepegawaianFragment.CustomOnClickListener());
//        spinner1.setOnItemSelectedListener(new PieChartKepegawaianFragment.CustomOnClickListener());
//    }
//
//    public class CustomOnClickListener implements AdapterView.OnItemSelectedListener {
//
//        @Override
//        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
//            index = spinner2.getSelectedItemPosition();
//            index_UnitKerja = spinner1.getSelectedItemPosition();
//            DrawChart();
//        }
//
//        @Override
//        public void onNothingSelected(AdapterView<?> adapterView) {
//
//        }
//    }
//
//    public class GetURLdata extends AsyncTask<String, Void, String> {
//
//        @Override
//        protected String doInBackground(String... url) {
//            ReadJSONUrl downloadURL = new ReadJSONUrl();
//            try {
//                urlData = downloadURL.readUrl(url[0]);
//                jsondata = new JSONObject(urlData);
//            } catch (IOException e) {
//                e.printStackTrace();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return urlData;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//            getDataChart();
//            DrawChart();
//        }
//    }
//}
