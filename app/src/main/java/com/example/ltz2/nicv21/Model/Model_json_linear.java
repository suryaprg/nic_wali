package com.example.ltz2.nicv21.Model;

/**
 * Created by LALA on 16/01/2019.
 */

public class Model_json_linear {
    private int th;
    private String jml;

    public int getTh(){
        return th;
    }

    public void setTh(int th) {
        this.th = th;
    }

    public String getJml() {
        return jml;
    }

    public void setJml(String jml) {
        this.jml = jml;
    }

    public Model_json_linear(int th, String jml) {
        this.th = th;
        this.jml = jml;
    }
}
