package com.example.ltz2.nicv21;

public class URLCollection {

    public static final String DATA_SOURCE_RS = "https://spm.malangkota.go.id/monitoring/ws/rs_data";
    public static final String DATA_SOURCE_PENYAKIT = "https://spm.malangkota.go.id/monitoring/ws/penyakit_data";
    public static final String DATA_SOURCE_DINSOS = "https://spm.ma2langkota.go.id/monitoring/ws/dinsos_data";
    public static final String DATA_SOURCE_KEL = "https://spm.malangkota.go.id/monitoring/ws/kel_data";
    public static final String DATA_SOURCE_VERIF = "https://spm.malangkota.go.id/monitoring/ws/verif_data";
    public static final String DATA_SOURCE_KEPEGAWAIAN = "http://simpeg.malangkota.go.id/serv/get.php";
}
