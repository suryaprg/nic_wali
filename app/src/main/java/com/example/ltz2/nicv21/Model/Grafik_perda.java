package com.example.ltz2.nicv21.Model;

public class Grafik_perda {
    String tahun;
    String jumlah;

    public Grafik_perda(String tahun, String jumlah) {
        this.tahun = tahun;
        this.jumlah = jumlah;
    }

    public String getTahun() {
        return tahun;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }
}
