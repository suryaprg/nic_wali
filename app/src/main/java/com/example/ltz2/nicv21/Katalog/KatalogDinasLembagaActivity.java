package com.example.ltz2.nicv21.Katalog;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.LinearLayout;

import com.example.ltz2.nicv21.Barchart.BarchartKemasyarakatanActivity;
import com.example.ltz2.nicv21.R;

public class KatalogDinasLembagaActivity extends Activity {
    LinearLayout mLinear;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.katalog_dinas_lembaga_new);
        mLinear = (LinearLayout) findViewById(R.id.dinas_linear_grid);
        setSingleEvent(mLinear);
    }

    private void setSingleEvent(LinearLayout mLinear) {
        //Loop all child item of Main Grid
        for (int i = 0; i < mLinear.getChildCount(); i++) {
            //You can see , all child item is CardView , so we just cast object to CardView
            final CardView cardView = (CardView) mLinear.getChildAt(i);
            final int finalI = i;
            if (i == 0) {
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent();
                        i.setAction(Intent.ACTION_VIEW);
                        i.addCategory(Intent.CATEGORY_BROWSABLE);

                        i.setData(Uri.parse("http://ncc.sasindo.id/map?idsubmodule=18&selectlist%5Bkecamatan%5D%5B%5D=kecamatan&grouplist=kecamatan&joinlist%5B0%5D%5Bdest_table%5D=kecamatan&joinlist%5B0%5D%5Bdest_column%5D=idkecamatan&countlist%5B%5D=pendudukakhirbulanini_p&countlist%5B%5D=pendudukakhirbulanini_l"));
                        startActivity(i);
                    }
                });
            } else if (i == 1) {
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent();
                        i.setAction(Intent.ACTION_VIEW);
                        i.addCategory(Intent.CATEGORY_BROWSABLE);

                        i.setData(Uri.parse("http://ncc.sasindo.id/map?idsubmodule=18&selectlist%5Bkecamatan%5D%5B%5D=kecamatan&grouplist=kecamatan&joinlist%5B0%5D%5Bdest_table%5D=kecamatan&joinlist%5B0%5D%5Bdest_column%5D=idkecamatan&countlist%5B%5D=pendudukakhirbulanini_p&countlist%5B%5D=pendudukakhirbulanini_l"));
                        startActivity(i);
                    }
                });
            } else if (i == 2) {
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent();
                        i.setAction(Intent.ACTION_VIEW);
                        i.addCategory(Intent.CATEGORY_BROWSABLE);

                        i.setData(Uri.parse("https://bnn.malangkota.go.id/"));
                        startActivity(i);
                    }
                });
            } else if (i == 3) {
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent();
                        i.setAction(Intent.ACTION_VIEW);
                        i.addCategory(Intent.CATEGORY_BROWSABLE);

                        i.setData(Uri.parse("http://kpud-malangkota.go.id/"));
                        startActivity(i);
                    }
                });
            } else if (i == 4) {
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent();
                        i.setAction(Intent.ACTION_VIEW);
                        i.addCategory(Intent.CATEGORY_BROWSABLE);

                        i.setData(Uri.parse("https://disnaker.malangkota.go.id/database/data-pengangguran/"));
                        startActivity(i);
                    }
                });
            }
        }
    }
}
