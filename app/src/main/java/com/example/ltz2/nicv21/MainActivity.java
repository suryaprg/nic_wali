package com.example.ltz2.nicv21;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.ltz2.nicv21.Adapter.SessionManagement;
import com.example.ltz2.nicv21.Katalog.KatalogAdministrasiActivity;
import com.example.ltz2.nicv21.Katalog.KatalogDinasLembagaActivity;
import com.example.ltz2.nicv21.Katalog.KatalogIMMActivity;
import com.example.ltz2.nicv21.Katalog.KatalogKemasyarakatanActivity;
import com.example.ltz2.nicv21.Katalog.KatalogKepegawaianActivity;
import com.example.ltz2.nicv21.Katalog.KatalogKeuanganActivity;
import com.example.ltz2.nicv21.Katalog.KatalogKewilayahanActivity;
import com.example.ltz2.nicv21.Katalog.KatalogLegislasiActivity;
import com.example.ltz2.nicv21.Katalog.KatalogPelayananActivity;
import com.example.ltz2.nicv21.Katalog.KatalogPembangunanActivity;
import com.example.ltz2.nicv21.Katalog.KatalogSaranaActivity;


public class MainActivity extends Activity {
    GridLayout mainGrid;
    Button bt_md, buttonLogout;
    SessionManagement sessionManagement;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.katalog_main_new);


        mainGrid = (GridLayout)findViewById(R.id.mainGrid);
        setSingleEvent(mainGrid);

        buttonLogout = (Button) findViewById(R.id.buttonlogout);

        // Session class instance
        sessionManagement = new SessionManagement(getApplicationContext());

        // Button logout

        Toast.makeText(getApplicationContext(), "User Login Status: " + sessionManagement.isLoggedIn(), Toast.LENGTH_LONG).show();

        /**
         * Call this function whenever you want to check user login
         * This will redirect user to LoginActivity is he is not
         * logged in
         * */
        sessionManagement.checkIsLogin();
//        buttonLogout.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View arg0) {
//                // Clear the session data
//                // This will clear all session data and
//                // redirect user to LoginActivity
//                sessionManagement.logoutUser();
//            }
//        });
        buttonLogout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // Clear the session data
                // This will clear all session data and
                // redirect user to LoginActivity
                sessionManagement.logoutUser();
            }
        });

//    mapss = (Button) findViewById(R.id.mp);
//    mapss.setOnClickListener(new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            Intent i = new Intent();
//            i.setAction(Intent.ACTION_VIEW);
//            i.addCategory(Intent.CATEGORY_BROWSABLE);
//
//            i.setData(Uri.parse("https://www.google.com/maps/place/Pemerintah+Kota+Malang/@-7.978075,112.6316793,17z/data=!3m1!4b1!4m5!3m4!1s0x2dd6282395555555:0x7f9d150614c62b6e!8m2!3d-7.978075!4d112.633868"));
//            startActivity(i);
//        }
//    });
        bt_md = (Button) findViewById(R.id.bt_md);
        bt_md.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,KatalogIMMActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setSingleEvent(GridLayout mainGrid) {
        for (int i =0; i<mainGrid.getChildCount();i++){
            CardView cardView =(CardView)mainGrid.getChildAt(i);
            final int finalT = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (finalT == 1 ){
                        Intent intent = new Intent(MainActivity.this,KatalogKepegawaianActivity.class);
                        startActivity(intent);
                    }
                    else if (finalT == 0){
                        Intent intent = new Intent(MainActivity.this, KatalogPelayananActivity.class);
                        startActivity(intent);
                    }

                    else if (finalT == 2){
                        Intent intent = new Intent(MainActivity.this, KatalogAdministrasiActivity.class);
                        startActivity(intent);
                    }

                    else if (finalT == 3){
                        Intent intent = new Intent(MainActivity.this, KatalogLegislasiActivity.class);
                        startActivity(intent);
                    }

                    else if (finalT == 4){
                        Intent intent = new Intent(MainActivity.this, KatalogPembangunanActivity.class);
                        startActivity(intent);
                    }

                    else if (finalT == 5){
                        Intent intent = new Intent(MainActivity.this, KatalogDinasLembagaActivity.class);
                        startActivity(intent);
                    }

                    else if (finalT == 6 ){
                        Intent intent = new Intent(MainActivity.this,KatalogKeuanganActivity.class);
                        startActivity(intent);
                    }

                    else if (finalT == 7){
                        Intent intent = new Intent(MainActivity.this, KatalogKewilayahanActivity.class);
                        startActivity(intent);
                    }

                    else if (finalT == 8 ){
                        Intent intent = new Intent(MainActivity.this,KatalogKemasyarakatanActivity.class);
                        startActivity(intent);
                    }

                    else if (finalT == 10){
                        Intent intent = new Intent(MainActivity.this, KatalogSaranaActivity.class);
                        startActivity(intent);
                    }

//                    else if (finalT == 10){
//                        Intent intent = new Intent(MainActivity.this,KatalogIMMActivity.class);
//                        startActivity(intent);
//                    }
                }
            });
        }
    }
}

