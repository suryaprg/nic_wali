package com.example.ltz2.nicv21.Barchart;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.ltz2.nicv21.Adapter.ReadJSONUrl;
import com.example.ltz2.nicv21.Model.Grafik_perda;
import com.example.ltz2.nicv21.R;
import com.example.ltz2.nicv21.URLCollection;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;


public class BarchartKemasyarakatanActivity extends Activity {
    JSONArray jsondata;
    BarChart barChart;
    String urlData;
    ArrayList<String> label;
    ArrayList<BarEntry> barEntries;
    TableLayout main_tabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.barchart_graph);
        String info = getIntent().getStringExtra("info");
        int index = getIntent().getIntExtra("index", 9);
        TextView judulChart = (TextView) findViewById(R.id.judulChart);
        judulChart.setText(info);
        main_tabel = (TableLayout) findViewById(R.id.tablelayoutid);
        barChart = new BarChart(this);
//        new GetURLdata().execute("https://spm.malangkota.go.id/monitoring/ws/rs_data");
//        public void loop_graph(){
//            ReadJSONUrl downloadURL = new ReadJSONUrl();
//            try {
//
//                urlData = downloadURL.readUrl(url[0]);
//                jsondata = new JSONArray(urlData);
////                JSONObject data_json = new JSONObject().getJSONObject(val_kec);
////                Integer leng_data = data_json.length();
//
//                Iterator iterator = data_json.keys();
//                int i = 0;
//
//                while (iterator.hasNext()) {
//                    String key = (String) iterator.next();
//
//                    String val = (String) data_json.getString(key);
//                    Log.e("table_perda", "onCreate : jml_record: " + key + "value: " + val);
//
//                    TableRow row = new TableRow(this);
//
//                    TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT);
//                    row.setLayoutParams(lp);
//
//                    tv_th = new TextView(this);
//                    tv_val = new TextView(this);
//
//
//                    tv_th.setText(key);
//                    tv_th.setWidth(250);
//                    tv_th.setBackgroundResource(R.color.white);
//
//
//                    tv_val.setText(val);
//                    tv_val.setWidth(250);
//                    tv_val.setBackgroundResource(R.color.white);
//
//                    row.addView(tv_th);
//                    row.addView(tv_val);
//                    main_table.addView(row, i);
//
//
//                    i++;
////                dbGrafik_perda.add(new Grafik_perda(key,val));
//
//                }
//                TableRow row1 = new TableRow(this);
//                TableRow.LayoutParams lp2 = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT);
//                row1.setLayoutParams(lp2);
//                TextView tv_a = new TextView(this);
//                TextView tv_b = new TextView(this);
//
//                tv_a.setText("tahun");
//                tv_a.setWidth(250);
//                tv_a.setBackgroundResource(R.color.honeydew);
//
//                tv_b.setText("jumlah");
//                tv_b.setWidth(250);
//                tv_b.setBackgroundResource(R.color.teal);
//
//                row1.addView(tv_a);
//                row1.addView(tv_b);
//                main_table.addView(row1, 0);
//
//                Log.e("table_perda", "onCreate : jml_record: " + leng_data);
//
//            } catch (JSONException e) {
//                Log.e("MainActivity", "Oncreate : " + e.getMessage());
//            }
//        }

        switch (index) {
            case 0:
                new GetURLdata().execute(URLCollection.DATA_SOURCE_RS);
                break;
            case 1:
                new GetURLdata().execute(URLCollection.DATA_SOURCE_PENYAKIT);
                break;
            case 2:
                new GetURLdata2().execute(URLCollection.DATA_SOURCE_DINSOS);
                break;
            case 3:
                new GetURLdata2().execute(URLCollection.DATA_SOURCE_KEL);
                break;
            case 4:
                new GetURLdata2().execute(URLCollection.DATA_SOURCE_VERIF);
                break;
        }
    }

    public void showChart2() {
        try {
            barChart = (BarChart) findViewById(R.id.bargraph);
            barChart.invalidate(); // refresh
            barEntries = new ArrayList<>();
            label = new ArrayList<>();
            for (int i = 0; i < jsondata.length(); i++) {
                barEntries.add(new BarEntry(i, (float) jsondata.getInt(i)));
//                label.add(jsondata.getJSONObject(i).getString("name"));
                String monthname = "";
                switch (i) {
                    case 0:
                        monthname = "Jan";
                        break;
                    case 1:
                        monthname = "Feb";
                        break;
                    case 2:
                        monthname = "Mar";
                        break;
                    case 3:
                        monthname = "Apr";
                        break;
                    case 4:
                        monthname = "Mei";
                        break;
                    case 5:
                        monthname = "Jun";
                        break;
                    case 6:
                        monthname = "Jul";
                        break;
                    case 7:
                        monthname = "Aug";
                        break;
                    case 8:
                        monthname = "Sep";
                        break;
                    case 9:
                        monthname = "Oct";
                        break;
                    case 10:
                        monthname = "Nov";
                        break;
                    case 11:
                        monthname = "Dec";
                        break;

                }
                label.add(monthname);
            }
            BarDataSet set = new BarDataSet(barEntries, "-");
            set.setColors(ColorTemplate.COLORFUL_COLORS);

            BarData data = new BarData(set);
            data.setValueTextSize(12f);
            data.setBarWidth(0.4f); // set custom bar width
            data.setValueFormatter(new IValueFormatter() {
                @Override
                public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                    return (int) value + "";
                }
            });
            barChart.setData(data);
            barChart.setFitBars(true); // make the x-axis fit exactly all bars
            barChart.notifyDataSetChanged();
            barChart.invalidate(); // refresh
            barChart.getXAxis().setDrawGridLines(false);
            barChart.getAxisLeft().setEnabled(false);
            barChart.getAxisRight().setEnabled(false);
            barChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(label));
            barChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
            barChart.getXAxis().setLabelCount(label.size());
            barChart.getXAxis().setTextSize(12f);
        } catch (Exception e) {
            Log.d("test exception", e.toString());
        }

    }

    public void showChart1() {
        try {
            barChart = (BarChart) findViewById(R.id.bargraph);
            barChart.invalidate(); // refresh
            barEntries = new ArrayList<>();
            label = new ArrayList<>();
            for (int i = 0; i < jsondata.length(); i++) {
                barEntries.add(new BarEntry(i, (float) jsondata.getJSONObject(i).getJSONArray("data").getInt(0)));
                label.add(jsondata.getJSONObject(i).getString("name"));
            }
            BarDataSet set = new BarDataSet(barEntries, "Data Jumlah Pasien");
            set.setColors(ColorTemplate.COLORFUL_COLORS);

            BarData data = new BarData(set);
            data.setValueTextSize(12f);
            data.setBarWidth(0.4f); // set custom bar width
            barChart.setData(data);
            barChart.setFitBars(true); // make the x-axis fit exactly all bars
            barChart.notifyDataSetChanged();
            barChart.invalidate(); // refresh
            barChart.getXAxis().setDrawGridLines(false);
            barChart.getAxisLeft().setEnabled(false);
            barChart.getAxisRight().setEnabled(false);
            barChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(label));
            barChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
            barChart.getXAxis().setLabelCount(label.size());
            barChart.getXAxis().setTextSize(12f);

            if (jsondata.length() == 7) {
                barChart.getXAxis().setLabelRotationAngle(-90);
            } else {
                barChart.getXAxis().setLabelRotationAngle(0);
            }
        } catch (Exception e) {
            Log.d("test exception", e.toString());
        }

    }

    public class GetURLdata extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {
            ReadJSONUrl downloadURL = new ReadJSONUrl();
            try {
                urlData = downloadURL.readUrl(url[0]);
                jsondata = new JSONArray(urlData);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return urlData;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//            getData();
            showChart1();
            barChart.getDescription().setEnabled(false);
            barChart.animateY(3000);
        }
    }

    public class GetURLdata2 extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {
            ReadJSONUrl downloadURL = new ReadJSONUrl();
            try {
                urlData = downloadURL.readUrl(url[0]);
                jsondata = new JSONArray(urlData);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return urlData;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//            getData();
            showChart2();
            barChart.getDescription().setEnabled(false);
            barChart.animateY(1500);
        }
    }
}