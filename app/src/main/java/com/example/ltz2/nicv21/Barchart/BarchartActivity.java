package com.example.ltz2.nicv21.Barchart;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.example.ltz2.nicv21.Adapter.KeuanganDataChart;
import com.example.ltz2.nicv21.Model.PaguPendatapanModel;
import com.example.ltz2.nicv21.Model.RealisasiPendapatanModel;
import com.example.ltz2.nicv21.R;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import org.json.JSONArray;
import org.json.JSONObject;

import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class BarchartActivity extends Activity {
    BarChart barChart;
    ArrayList<String> label;
    ArrayList<BarEntry> barEntries;
    ArrayList<PaguPendatapanModel> pagupendapatanlist;
    ArrayList<RealisasiPendapatanModel> realisasipendapatanlist;
    double jmlpagu = 0;
    double jmlrealisasi = 0;
    double jmlrealisasibln[] = new double[3];
    JSONObject jsondata;
    int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.barchart_graph);
        barChart = (BarChart) findViewById(R.id.bargraph);
        barEntries = new ArrayList<>();
        label = new ArrayList<>();
        KeuanganDataChart keuangandtc = new KeuanganDataChart(this);
        hitungChart(this, keuangandtc);
//        for (int i = 0; i < pagupendapatanlist.size(); i++) {
//            double value = pagupendapatanlist.get(i).getNominal();
//            barEntries.add(new BarEntry(i, (float) (value * 100 / jmlpagu)));
////            for (int j = 0; j <pagupendapatanlist.size(); j++) {
////                if(realisasipendapatanlist.get(i).getId_skpd()==pagupendapatanlist.get(j).getId_skpd()){
////                    label.add(pagupendapatanlist.get(j).getNama_skpd());
////                }
////            }
//            label.add(pagupendapatanlist.get(i).getNama_skpd());
//        }

        //{ Data Total Pagu dan Total Realisasi
//        barEntries.add(new BarEntry(0, (float)jmlpagu));
//        barEntries.add(new BarEntry(1, (float)jmlrealisasi));
//        label.add("Pagu Pendapatan");
//        label.add("Realisasi Pendapatan");
//        BarDataSet set = new BarDataSet(barEntries, "Data Dalam bentuk Rupiah");
//        BarData data = new BarData(set);
//        data.setBarWidth(0.4f); // set custom bar width
//        barChart.setData(data);
        //} Data Total Pagu dan Total Realisasi

        //{ Data Realisasi per Bulan
        barEntries.add(new BarEntry(0, (float) jmlrealisasibln[0]));
        barEntries.add(new BarEntry(1, (float) jmlrealisasibln[1]));
        barEntries.add(new BarEntry(2, (float) jmlrealisasibln[2]));
        label.add("Bulan 1");
        label.add("Bulan 2");
        label.add("Bulan 3");
        BarDataSet set = new BarDataSet(barEntries, "Data Dalam bentuk Rupiah");
        set.setValueTextSize(12f);
        BarData data = new BarData(set);
        data.setBarWidth(0.4f); // set custom bar width
        barChart.setData(data);
        //} Data Realisasi per Bulan

        barChart.setFitBars(true); // make the x-axis fit exactly all bars
        barChart.invalidate(); // refresh
        barChart.getXAxis().setDrawGridLines(false);
        barChart.getAxisLeft().setEnabled(false);
        barChart.getAxisRight().setEnabled(false);
        barChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(label));
        barChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        barChart.getXAxis().setLabelCount(label.size());
        barChart.getXAxis().setLabelRotationAngle(0f);
        barChart.getXAxis().setTextSize(12f);

        Log.d("bulan 3", "" + count);
    }

    public void createRandomBarGraph() {
//        barEntries = new ArrayList<>();
        label = new ArrayList<>();
//        for (int i = 0; i < pagupendapatanlist.size(); i++) {
//            double value = pagupendapatanlist.get(i).getNominal();
//            barEntries.add(new BarEntry((float) (value / jmlpagu * 100), i));
////            for (int j = 0; j <pagupendapatanlist.size(); j++) {
////                if(realisasipendapatanlist.get(i).getId_skpd()==pagupendapatanlist.get(j).getId_skpd()){
////                    label.add(pagupendapatanlist.get(j).getNama_skpd());
////                }
////            }
//            label.add(pagupendapatanlist.get(i).getNama_skpd());
//
//        }
//        BarDataSet barDataSet = new BarDataSet(barEntries, "Dalam Rupiah");
//        BarData barData = new BarData(barDataSet);
//
//        barChart.setData(barData);

    }

    public void hitungChart(Context context, KeuanganDataChart kdtc) {
        try {
            pagupendapatanlist = new ArrayList<>();
            realisasipendapatanlist = new ArrayList<>();
            jsondata = new JSONObject(kdtc.getJsonData(context));
            JSONArray jsonpagu = jsondata.getJSONArray("pagu_pendapatan");
            JSONArray jsonrealisasi = jsondata.getJSONArray("realisasi_pendapatan");
            Log.d("test", "" + jsonpagu.length());
            for (int i = 0; i < jsonpagu.length(); i++) {
                JSONObject temp = jsonpagu.getJSONObject(i);
                Double nominal = temp.getDouble("NOMINAL");
                int id = temp.getInt("ID_SKPD");
                String nama = temp.getString("NAMA_SKPD");
                pagupendapatanlist.add(new PaguPendatapanModel(nominal, id, nama));
                jmlpagu += nominal;
            }
            for (int i = 0; i < jsonrealisasi.length(); i++) {
                JSONObject temp = jsonrealisasi.getJSONObject(i);
                Double nominal = temp.getDouble("NOMINAL");
                int id = temp.getInt("ID_SKPD");
                int bulan = temp.getInt("BULAN");
                realisasipendapatanlist.add(new RealisasiPendapatanModel(nominal, id, bulan));
                jmlrealisasi += nominal;
                jmlrealisasibln[bulan - 1] += nominal;
                if (bulan == 3) {
                    count++;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("JSON Parser", "String data to json error");
        }
    }
}
