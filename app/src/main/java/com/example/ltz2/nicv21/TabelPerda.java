package com.example.ltz2.nicv21;


import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.ltz2.nicv21.Adapter.Json_Reader;
import com.example.ltz2.nicv21.Model.Grafik_perda;
import com.example.ltz2.nicv21.Model.Model_json_linear;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * Created by LALA on 16/01/2019.
 */

public class TabelPerda extends Activity {
    String str_json;
    InputStream is;
    TableLayout main_table;
    TableRow row;
    TextView tv_th, tv_val, tv_a, tv_b;
    BarChart barChart;
    ArrayList<Grafik_perda> dbGrafik_perda;
    ArrayList<String> label;
    ArrayList<BarEntry> barEntries;
    int index;

    ArrayList<Model_json_linear> data_list = new ArrayList<Model_json_linear>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.table_perda);
        String info = getIntent().getStringExtra("info");
        String index = getIntent().getStringExtra("index");
        TextView judulChart = (TextView) findViewById(R.id.judulChart);
        judulChart.setText(info);
        main_table = (TableLayout) findViewById(R.id.table2);
        barChart = (BarChart) findViewById(R.id.bargraph);
        barEntries = new ArrayList<>();
        label = new ArrayList<>();
        dbGrafik_perda = new ArrayList<>();

        str_json = new Json_Reader().get_json_file(get_activity(), index);
        Log.e("tabel_perwal", str_json);

        str_json = new Json_Reader().get_json_file(get_activity(), "perda.json");
        Log.e("tabel_perwal", str_json);
//        mLinear = (LinearLayout) findViewById(R.id.legislasi_linear_grid);
//        setSingleEvent(mLinear);
        try {
            JSONObject data_json = new JSONObject(str_json);
            Integer leng_data = data_json.length();

            Iterator iterator = data_json.keys();
            int i = 0;
            while (iterator.hasNext()) {
                String key = (String) iterator.next();

                String val = (String) data_json.getString(key);
                Log.e("tabel_perwal", "onCreate : jml_record: " + key + "value: " + val);

                TableRow row = new TableRow(this);

                TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT);
                row.setLayoutParams(lp);

                tv_th = new TextView(this);
                tv_val = new TextView(this);


                tv_th.setText(key);
                tv_th.setWidth(250);
                tv_th.setBackgroundResource(R.color.white);


                tv_val.setText(val);
                tv_val.setWidth(250);
                tv_val.setBackgroundResource(R.color.white);

                row.addView(tv_th);
                row.addView(tv_val);
                main_table.addView(row, i);

                i++;
                dbGrafik_perda.add(new Grafik_perda(key,val));

            }
            TableRow row1 = new TableRow(this);
            TableRow.LayoutParams lp2 = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT);
            row1.setLayoutParams(lp2);
            tv_a = new TextView(this);
            tv_b = new TextView(this);

            tv_a.setText("Tahun");
            tv_a.setWidth(250);
            tv_a.setBackgroundResource(R.color.honeydew);

            tv_b.setText("Jumlah");
            tv_b.setWidth(250);
            tv_b.setBackgroundResource(R.color.teal);

            row1.addView(tv_a);
            row1.addView(tv_b);
            main_table.addView(row1, 0);

            Log.e("tabel_perwal", "onCreate : jml_record: " + leng_data);
            DrawChart();

        } catch (JSONException e) {
            Log.e("MainActivity", "Oncreate : " + e.getMessage());
        }
    }
    public void DrawChart() {
        barEntries.clear();
        label.clear();
        barChart.invalidate();
        barChart.clear();

        for (int i = 0; i< dbGrafik_perda.size(); i++){
//        barEntries.add(new BarEntry(0, Float.parseFloat(dbGrafik_pemakaman.get(i).getTahun())));
            barEntries.add(new BarEntry(i, Float.parseFloat(dbGrafik_perda.get(i).getJumlah())));
//        label.add("2017");
//        label.add("2018");

            label.add(dbGrafik_perda.get(i).getTahun());
        }
        BarDataSet set = new BarDataSet(barEntries, "Data Jumlah Pegawai");
        set.setColors(ColorTemplate.COLORFUL_COLORS);
        BarData data = new BarData(set);
        data.setValueFormatter(new IValueFormatter(){
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                return (int)value + "";
            }
        });
        data.setValueTextSize(12f);
        data.setBarWidth(0.7f); // set custom bar width
        barChart.setData(data);
        Legend legend = barChart.getLegend();
//        label.toArray(new String[label.size()]);
//        legend.setCustom();
        //label variabel
        legend.setEnabled(false);

        barChart.setFitBars(true); // make the x-axis fit exactly all bars

        barChart.notifyDataSetChanged();
        barChart.invalidate(); // refresh

        //grid line
        barChart.getXAxis().setDrawGridLines(false);
        barChart.getAxisLeft().setEnabled(false);
        barChart.getAxisRight().setEnabled(false);


        //set variable label array x or y
        barChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(label));

        //set label on chart
        barChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);

        //jml label view
        barChart.getXAxis().setLabelCount(label.size());
        barChart.getXAxis().setTextSize(12f);
        barChart.getXAxis().setLabelRotationAngle(-90);

        //label bawah
        barChart.getDescription().setEnabled(true);
        barChart.animateY(3000);
//        Log.d("CEK DATA JSON", "" + (float) dbGrafik_pemakaman.get(0).getJml_gol_3());
    }


    public Context get_activity() {
        Context context = this;
        return context;
    }
}
